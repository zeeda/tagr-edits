var main = (function(){
    var INIT_CAROUSEL = function(){
        $('.__js_benefits_carousel').owlCarousel({
            loop: true,
            margin: 10,
            responsiveClass: true,
            rtl: true,
            responsive: {
                0: {
                    items: 2,
                    nav: false,
                    dots: true
                },
                600: {
                    items: 2,
                    nav: false
                },
                1000: {
                    items: 2,
                    nav: true,
                    loop: false
                }
            }
        })

        $('.__js_bundles_carousel').owlCarousel({
            loop: true,
            center: true,
            responsiveClass: true,
            margin: 64,
            rtl: true,
            dots: false,
            responsive: {
                0: {
                    items: 1,
                    center: false,
                    loop: false,
                    nav: false,
                    margin: 0,
                    dots: true,
                },
                600: {
                    items: 2,
                    nav: false,
                    margin: 32
                },
                1000: {
                    items: 2,
                    nav: true,
                    loop: false,
                    margin: 64
                }
            }
        })
    }
    var INIT_BUNDLE_TABLE = function(){
        var row = $('.bundles .table tbody tr');
        row.on('click', function (e) {
            var button = e.target;
            if($(button).hasClass('accordion-button')) {
                if($(button).attr('aria-expanded') == 'true'){
                    $(this).addClass('expanded');
                }else{
                    $(this).removeClass('expanded');
                }
                
            }
        })
    }
    return{
        init: function init(){
            INIT_BUNDLE_TABLE();
            INIT_CAROUSEL();
        }
    }
})()
main.init();
$(document).ready(function(){
})
